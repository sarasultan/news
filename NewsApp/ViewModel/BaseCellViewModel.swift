//
//  BaseCellViewModel.swift
//  NewsApp
//
//  Created by Sara Sultan on 09/09/2021.
//

import Foundation
import UIKit


protocol BaseCellViewModel {
    var type: UITableViewCell.Type { get }
}
