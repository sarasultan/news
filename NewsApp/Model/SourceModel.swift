//
//  SourceModel.swift
//  NewsApp
//
//  Created by Sara Sultan on 08/09/2021.
//

import Foundation

struct SourceModel: Codable {
    let id: String?
    let name: String?
}
