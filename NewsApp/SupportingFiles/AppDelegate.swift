//
//  AppDelegate.swift
//  NewsApp
//
//  Created by Sara Sultan on 08/09/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if let navigationController = window?.rootViewController as? UINavigationController {
            if let newsFeedViewController = navigationController.topViewController as? NewsFeedsViewController {
                newsFeedViewController.setupViewModel(viewModel: NewsFeedsViewModel())
            }
        }
        return true
    }
}

